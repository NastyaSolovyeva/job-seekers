const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const cssnano = require('cssnano');
const webpack = require('webpack');

process.traceDeprecation = true;
module.exports = {
  entry: [path.resolve(__dirname, '../src/app.tsx')],
  output: {
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/',
    filename: '[name].[hash].js'
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: 'html-loader',
        include: path.resolve(__dirname, '../public'),
        exclude: path.resolve(__dirname, '../node_modules'),
        options: {
          minimize: true
        }
      },
      {
        test: /\.(ts|tsx)$/,
        include: path.resolve(__dirname, '../src'),
        exclude: [
          path.resolve(__dirname, '../node_modules'),
          path.resolve(__dirname, '../dist'),
          path.resolve(__dirname, '../webpack')
        ],
        use: [
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
              experimentalWatchApi: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, '../src'),
        exclude: path.resolve(__dirname, '../node_modules'),
        use: [
          ExtractTextPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]__[local]___[hash:base64:5]'
              },
              sourceMap: true,
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: () => [cssnano()]
            }
          },
          'sass-loader'
        ]
      }
    ]
  },
  resolve: {
    modules: ['node_modules', 'src'],
    extensions: ['index.js', 'index.jsx', 'index.ts', 'index.tsx', '.js', '.jsx', '.json', '.scss', '.ts', '.tsx'],
    alias: {
      '@common': path.resolve(__dirname, '../src/common/'),
      '@routes': path.resolve(__dirname, '../src/routes/'),
      '@services': path.resolve(__dirname, '../src/services/'),
      '@helpers': path.resolve(__dirname, '../src/helpers/index.ts'),
      '@root': path.resolve(__dirname, '../src/')
    }
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../public/index.html')
    }),
    new ExtractTextPlugin({
      filename: '[name].[hash].css'
    }),
    new webpack.optimize.ModuleConcatenationPlugin()
  ]
};
