export function classGenerator(styles: { [className: string]: string } = {}): Function {
  return (...classNames: Array<string>): string =>
    classNames
      .map((className: string): string => styles[className] || className || '')
      .filter((className: string): boolean => className.length > 0)
      .join(' ');
}
