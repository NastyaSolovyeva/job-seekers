import React, { FC, memo } from 'react';

import { classGenerator } from '@helpers';

import UserRow from './row';
import { PropsType } from './modues';
import styles from './styles.scss';

const c = classGenerator(styles);

const UserTable: FC<PropsType> = (props) => {
  const { userList } = props;

  return (
    <div className={c('container')}>
      <div className={c('table__wrapper')}>
        <table className={c('table')}>
          <thead className={c('table__head')}>
            <tr className={c('table__head__row')}>
              <th className={c('table__head__item')}>Name</th>
              <th className={c('table__head__item')}>Email</th>
              <th className={c('table__head__item')}>Phone</th>
              <th className={c('table__head__item')}>Company</th>
              <th className={c('table__head__item')}>Last contact</th>
            </tr>
          </thead>
          <tbody className={c('table__body')}>
            {userList.map((item) => <UserRow key={item.name} {...item} />)}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default memo(UserTable);
