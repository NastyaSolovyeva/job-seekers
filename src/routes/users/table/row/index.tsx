import React, { FC, memo, useCallback } from 'react';
import { useHistory } from 'react-router';
import moment from 'moment';

import { classGenerator } from '@helpers';

import { PropsType } from './modues';
import styles from '../styles.scss';

const c = classGenerator(styles);

const UserRow: FC<PropsType> = (props) => {
  const { id, name, email, phone, company, lastContact } = props;
  const history = useHistory();
  const avatar = 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png';

  const navigateToProfile = useCallback(() => {
    history.push(`user/${id}`);
  }, [history, id]);

  return (
    <tr className={c('table__body__row')}>
      <td className={c('table__body__item')} onClick={navigateToProfile}>
        <div className={c('table__body__item_avatar')}>
          <img src={avatar} alt="" />
          <span>{name}</span>
        </div>
      </td>
      <td className={c('table__body__item')}>{email}</td>
      <td className={c('table__body__item')}>{phone}</td>
      <td className={c('table__body__item')}>{company.name}</td>
      <td className={c('table__body__item')}>{moment(lastContact || new Date()).format('DD.MM.YYYY')}</td>
    </tr>
  );
};

export default memo(UserRow);
