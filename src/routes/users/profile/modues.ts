import { UserType } from '@services/users/models';
import { COMPANY, EMAIL, PHONE, USERNAME, WEBSITE, NAME } from '@routes/users/profile/constants';

export type PropsType = {
  userList: UserType[]
}

export type ProfileValuesType =
  typeof NAME |
  typeof USERNAME |
  typeof EMAIL |
  typeof PHONE |
  typeof WEBSITE |
  typeof COMPANY;

export type ProfileFormValuesType = {
  [key in ProfileValuesType]: string
};
