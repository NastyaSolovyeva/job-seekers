import React, { FC, memo, useCallback, useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';

import { classGenerator } from '@helpers';
import { getUserById } from '@services/users/actions';
import { RootStateType } from '@services/modules';
import Loader from '@common/loader';
import FormField from '@common/formField';
import { useForm } from '@routes/users/profile/customHooks';
import Button from '@common/button';

import { COMPANY, EMAIL, NAME, PHONE } from './constants';
import styles from './styles.scss';

const c = classGenerator(styles);

const Profile: FC = () => {
  const { userProfile, usersLoading } = useSelector((state: RootStateType) => state.usersReducer);
  const [isEdit, setIsEdit] = useState(false);
  const [form, setForm, setDefaultValues] = useForm(userProfile);

  const dispatch = useDispatch();
  const match = useRouteMatch<{ id: string }>();

  useEffect(() => {
    const { id } = match.params;

    dispatch(getUserById(Number(id)));
  }, [match, dispatch]);

  const formChangeHandler = useCallback((field: string, value: string) => {
    setForm((oldForm) => ({
      ...oldForm,
      [field]: value
    }));
  }, [setForm]);

  const startEditMode = useCallback(() => {
    setIsEdit(true);
  }, [setIsEdit]);

  const resetHandler = useCallback(() => {
    setDefaultValues();
    setIsEdit(false);
  }, [setDefaultValues]);

  const saveHandler = useCallback(() => {
    // save actions
    setIsEdit(false);
  }, [setIsEdit]);

  return (
    <div>
      {usersLoading ? <Loader />
        : userProfile && (
        <div className={c('profile')}>
          <div className={c('profile__card')}>
            <img
              className={c('profile__avatar')}
              src="https://iupac.org/wp-content/uploads/2018/05/default-avatar.png"
              alt=""
            />
            <div className={c('profile__text')}>{userProfile.name}</div>
            <div className={c('profile__text')}>@{userProfile.username}</div>
            <div className={c('profile__text', 'profile__text_light')}>{userProfile.phone}</div>
            <div className={c('profile__text', 'profile__text_light')}>{userProfile.email}</div>
          </div>

          <div className={c('form')}>
            <div className={c('form__body')}>
              <FormField
                value={form[NAME]}
                name={NAME}
                onChange={formChangeHandler}
                title="Name"
                placeholder="Name"
                isEdit={isEdit}
                error={form[NAME] ? '' : 'Not empty field'}
              />
              <FormField
                value={form[EMAIL]}
                name={EMAIL}
                onChange={formChangeHandler}
                title="Email"
                placeholder="Email"
                isEdit={isEdit}
                error={form[EMAIL] ? '' : 'Not empty field'}
              />
              <FormField
                value={form[PHONE]}
                name={PHONE}
                onChange={formChangeHandler}
                title="Phone"
                placeholder="Phone"
                isEdit={isEdit}
                error={form[PHONE] ? '' : 'Not empty field'}
              />
              <FormField
                value={form[COMPANY]}
                name={COMPANY}
                onChange={formChangeHandler}
                title="Company"
                placeholder="Company"
                isEdit={isEdit}
              />
            </div>
            <div className={c('form__footer')}>
              {isEdit ? (
                <>
                  <Button className={c('form__footer-btn', 'left-btn')} appearance="secondary" onClick={resetHandler}>Reset</Button>
                  <Button className={c('form__footer-btn')} appearance="primary" onClick={saveHandler}>Save</Button>
                </>
              ) : <Button className={c('edit-btn')} onClick={startEditMode}>Edit</Button>}
            </div>
          </div>
        </div>
        )}
    </div>
  );
};

export default memo(Profile);
