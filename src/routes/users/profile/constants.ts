export const NAME = 'name';
export const USERNAME = 'username';
export const EMAIL = 'email';
export const PHONE = 'phone';
export const WEBSITE = 'website';
export const COMPANY = 'company';
