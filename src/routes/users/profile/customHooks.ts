import { useState, Dispatch, SetStateAction, useMemo, useEffect, useCallback } from 'react';

import { ProfileFormValuesType } from '@routes/users/profile/modues';
import { UserType } from '@services/users/models';
import { COMPANY, EMAIL, NAME, PHONE, USERNAME, WEBSITE } from '@routes/users/profile/constants';

const defaultSignalValue = (user: UserType | null): ProfileFormValuesType => ({
  [NAME]: user?.name || '',
  [USERNAME]: user?.username || '',
  [EMAIL]: user?.email || '',
  [PHONE]: user?.phone || '',
  [WEBSITE]: user?.website || '',
  [COMPANY]: user?.company.name || ''
});

export const useForm = (user: UserType | null): [
  ProfileFormValuesType,
  Dispatch<SetStateAction<ProfileFormValuesType>>,
  () => void
] => {
  const defaultFormValues = useMemo(() => defaultSignalValue(user), [user]);

  const [form, setForm] = useState<ProfileFormValuesType>(defaultFormValues);

  useEffect(() => {
    setForm(defaultFormValues);
  }, [defaultFormValues]);

  const setDefaultValues = useCallback(() => setForm(defaultFormValues), [defaultFormValues]);

  return [form, setForm, setDefaultValues];
};
