import React, { ChangeEvent, FC, memo, useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getUsers } from '@services/users/actions';
import { RootStateType } from '@services/modules';
import { Loader } from '@common/loader';
import Header from '@common/header';
import { classGenerator } from '@helpers';
import Period from '@routes/users/filter';
import { DateIntervalType } from '@routes/users/filter/models';
import { useFilteredUsers } from '@routes/users/layout/customHooks';

import styles from './styles.scss';
import UserTable from '../table';

const c = classGenerator(styles);

const UsersLayout: FC = () => {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const { users: usersData, usersLoading } = useSelector((state: RootStateType) => state.usersReducer);
  const [users, filteredUsers, setFilteredUsers] = useFilteredUsers(usersData);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const filterByDate = useCallback((interval: DateIntervalType) => {
    const startTime = new Date(interval.startDate).getTime();
    const endTime = new Date(interval.endDate).getTime();

    const newUsers = users.filter((item) => {
      const currTime = item.lastContact ? new Date(item.lastContact).getTime() : 0;
      return currTime >= startTime && currTime <= endTime;
    });
    setFilteredUsers(newUsers);
  }, [setFilteredUsers, users]);

  const periodChangeHandler = useCallback((interval: DateIntervalType) => {
    setStartDate(interval.startDate);
    setEndDate(interval.endDate);

    filterByDate(interval);
  }, [filterByDate]);

  const searchInputChangeHandler = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.currentTarget;

    const newUsers = users.filter((item) => item.name.toLowerCase().indexOf(value) !== -1);
    setFilteredUsers(newUsers);
  }, [setFilteredUsers, users]);

  return (
    <div className={c('users-container')}>
      <Header title="Candidates" />

      <div className={c('filters')}>
        <Period startDate={startDate} endDate={endDate} onChange={periodChangeHandler} />
        <input className={c('search-input')} onChange={searchInputChangeHandler} placeholder="Search" />
      </div>

      {usersLoading ? <Loader />
        : filteredUsers && !!filteredUsers.length ? (
          <>
            <UserTable userList={filteredUsers} />
          </>
        ) : <span>Empty Data</span>}
    </div>
  );
};

export default memo(UsersLayout);
