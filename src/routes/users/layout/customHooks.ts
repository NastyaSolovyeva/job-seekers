import { Dispatch, SetStateAction, useEffect, useState } from 'react';

import { UserType } from '@services/users/models';

export const useFilteredUsers = (usersData: UserType[]): [UserType[], UserType[], Dispatch<SetStateAction<UserType[]>>] => {
  const [users, setUsers] = useState<UserType[]>([]);
  const [filteredUsers, setFilteredUsers] = useState<UserType[]>([]);

  const generateLastContactDate = (): Date => {
    const dateFrom = new Date(2021, 1, 1).getTime();
    const dateTo = new Date().getTime();
    const randomDate = dateFrom + Math.floor((dateTo - dateFrom) * Math.random());

    return new Date(randomDate);
  };

  useEffect(() => {
    const updatedUsersData = usersData.map((item) => ({
      ...item,
      lastContact: generateLastContactDate()
    }));
    setUsers(updatedUsersData);
    setFilteredUsers(updatedUsersData);
  }, [usersData]);

  return [users, filteredUsers, setFilteredUsers];
};
