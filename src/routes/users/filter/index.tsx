import React, { FC, memo, useCallback } from 'react';
import DatePicker from 'react-datepicker';

import { classGenerator } from '@helpers';

import { PropsType } from './models';
import styles from './styles.scss';

const c = classGenerator(styles);

const Period: FC<PropsType> = (props) => {
  const { endDate, startDate, onChange } = props;

  const startDateChangeHandler = useCallback((date: Date) => {
    onChange({
      startDate: date,
      endDate
    });
  }, [onChange, endDate]);

  const endDateChangeHandler = useCallback((date: Date) => {
    onChange({
      startDate,
      endDate: date
    });
  }, [onChange, startDate]);

  return (
    <div className={c('period')}>
      <DatePicker
        selected={startDate}
        onChange={startDateChangeHandler}
        dateFormat="dd.MM.yyyy"
      />
      <span className={c('period__separator')}>—</span>
      <DatePicker
        selected={endDate}
        dateFormat="dd.MM.yyyy"
        onChange={endDateChangeHandler}
        maxDate={new Date()}
      />
    </div>
  );
};

export default memo(Period);
