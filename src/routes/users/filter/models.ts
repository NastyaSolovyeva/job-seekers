export type DateIntervalType = {
  startDate: Date,
  endDate: Date
}

export type PropsType = {
  startDate: Date,
  endDate: Date,
  onChange(interval: DateIntervalType): void
};
