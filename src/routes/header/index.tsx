import React, { memo } from 'react';

import { classGenerator } from '@helpers';

import styles from './styles.scss';

const c = classGenerator(styles);

const Header = () => (
  <div className={c('header')}>
    <a className={c('header__logo')} href="/">Job Seekers</a>
  </div>
);

export default memo(Header);
