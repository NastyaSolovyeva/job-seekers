import { StoreType as UserStoreType } from './users/models';

export type RootStateType = {
  usersReducer: UserStoreType
}
