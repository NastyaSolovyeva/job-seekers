import { call, put, takeLatest } from 'redux-saga/effects';
import { Effect } from '@redux-saga/core/effects';

import { UsersActionEnum } from './constants';
import { getUserByIdApi, getUsersApi } from './api';
import { getUserByIdFailure, getUserByIdSuccess, getUsersFailure, getUsersSuccess } from './actions';
import { GetUserByIdActionType } from './models';

export function* getUsersSaga(): Generator<any, any, any> {
  try {
    const users = yield call(() => getUsersApi());

    yield put(getUsersSuccess(users));
  } catch (error) {
    yield put(getUsersFailure());
  }
}

export function* getUserByIdSaga(action: GetUserByIdActionType): Generator<any, any, any> {
  try {
    const users = yield call(() => getUserByIdApi(action.id));

    yield put(getUserByIdSuccess(users));
  } catch (error) {
    yield put(getUserByIdFailure());
  }
}

export const usersSaga: Array<Effect> = [
  takeLatest(UsersActionEnum.GET_USERS, getUsersSaga),
  takeLatest(UsersActionEnum.GET_USER_BY_ID, getUserByIdSaga)
];
