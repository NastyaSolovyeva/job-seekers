import { UsersActionEnum } from './constants';

export type UserType = {
  id: number,
  name: string,
  username: string,
  email: string,
  phone: string,
  address: UserAddressType,
  website: string,
  lastContact?: Date,
  company: CompanyType
};

export type UserAddressType = {
  street: string,
  suite: string,
  city: string,
  zipcode: string
};

export type CompanyType = {
  name: string,
  catchPhrase: string,
  bs: string
};

export type StoreType = {
  users: UserType[],
  usersLoading: boolean,
  userProfile: null | UserType,
  userProfileLoading: boolean
};


// GET USERS
export type GetUsersActionType = {
  type: typeof UsersActionEnum.GET_USERS
};
export type GetUsersSuccessActionType = {
  type: typeof UsersActionEnum.GET_USERS_SUCCESS,
  payload: {
    users: UserType[]
  }
};
export type GetUsersFailureActionType = {
  type: typeof UsersActionEnum.GET_USERS_FAILURE
};

// GET_USER_BY_ID
export type GetUserByIdActionType = {
  type: typeof UsersActionEnum.GET_USER_BY_ID,
  id: number
};
export type GetUserByIdSuccessActionType = {
  type: typeof UsersActionEnum.GET_USER_BY_ID_SUCCESS,
  payload: {
    user: UserType
  }
};
export type GetUserByIdFailureActionType = {
  type: typeof UsersActionEnum.GET_USER_BY_ID_FAILURE
};

export type UsersActionTypes =
  GetUsersActionType |
  GetUsersSuccessActionType |
  GetUsersFailureActionType |

  GetUserByIdActionType |
  GetUserByIdSuccessActionType |
  GetUserByIdFailureActionType;
