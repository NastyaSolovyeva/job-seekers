import { StoreType, UsersActionTypes } from './models';
import { UsersActionEnum } from './constants';

export const initialState: StoreType = {
  users: [],
  usersLoading: false,
  userProfile: null,
  userProfileLoading: false
};

export default function usersReducer(state: StoreType = initialState, action: UsersActionTypes): StoreType {
  switch (action.type) {
    case UsersActionEnum.GET_USERS: {
      return {
        ...state,
        usersLoading: true
      };
    }
    case UsersActionEnum.GET_USERS_SUCCESS: {
      const { users } = action.payload;

      return {
        ...state,
        users,
        usersLoading: false
      };
    }
    case UsersActionEnum.GET_USERS_FAILURE: {
      return {
        ...state,
        usersLoading: false
      };
    }

    case UsersActionEnum.GET_USER_BY_ID: {
      return {
        ...state,
        userProfileLoading: true
      };
    }
    case UsersActionEnum.GET_USER_BY_ID_SUCCESS: {
      const { user } = action.payload;

      return {
        ...state,
        userProfile: user,
        userProfileLoading: false
      };
    }
    case UsersActionEnum.GET_USER_BY_ID_FAILURE: {
      return {
        ...state,
        userProfileLoading: false
      };
    }

    default:
      return state;
  }
}
