// GET USERS
import { UsersActionEnum } from './constants';
import {
  GetUserByIdActionType,
  GetUserByIdFailureActionType,
  GetUserByIdSuccessActionType,
  GetUsersActionType,
  GetUsersFailureActionType,
  GetUsersSuccessActionType,
  UserType
} from './models';

// GET_USERS
export function getUsers(): GetUsersActionType {
  return {
    type: UsersActionEnum.GET_USERS
  };
}
export function getUsersSuccess(users: UserType[]): GetUsersSuccessActionType {
  return {
    type: UsersActionEnum.GET_USERS_SUCCESS,
    payload: {
      users
    }
  };
}
export function getUsersFailure(): GetUsersFailureActionType {
  return {
    type: UsersActionEnum.GET_USERS_FAILURE
  };
}

// GET_USER_BY_ID
export function getUserById(id: number): GetUserByIdActionType {
  return {
    type: UsersActionEnum.GET_USER_BY_ID,
    id
  };
}
export function getUserByIdSuccess(user: UserType): GetUserByIdSuccessActionType {
  return {
    type: UsersActionEnum.GET_USER_BY_ID_SUCCESS,
    payload: {
      user
    }
  };
}
export function getUserByIdFailure(): GetUserByIdFailureActionType {
  return {
    type: UsersActionEnum.GET_USER_BY_ID_FAILURE
  };
}
