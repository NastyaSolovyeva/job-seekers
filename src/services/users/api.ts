import { UserType } from './models';

function getUsersApi(): Promise<UserType[]> {
  const url = 'https://jsonplaceholder.typicode.com/users';

  return fetch(url, {
    method: 'GET',
    body: undefined,
    headers: {}
  }).then((res: Response): Promise<UserType[]> => res.json());
}

function getUserByIdApi(userId: number): Promise<UserType> {
  const url = `https://jsonplaceholder.typicode.com/users/${userId}`;

  return fetch(url, {
    method: 'GET',
    body: undefined,
    headers: {}
  }).then((res: Response): Promise<UserType> => res.json());
}

export {
  getUsersApi,
  getUserByIdApi
};
