import { all } from 'redux-saga/effects';
import { Effect } from '@redux-saga/core/effects';

import { usersSaga } from './users/middleware';

const sagas: Array<Effect> = [];

export function* watchSagas(): Generator<any, any, any> {
  yield all(
    sagas.concat(
      usersSaga
    )
  );
}
