export type PropsType = {
  name: string,
  value: string,
  title: string,
  isEdit: boolean,
  error?: string,
  placeholder?: string,
  disabled?: boolean,
  onChange(field: string, value: string): void
}
