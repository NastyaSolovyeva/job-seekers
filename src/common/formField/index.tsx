import React, { FC, ChangeEvent, memo, useCallback } from 'react';

import { classGenerator } from '@helpers';

import { PropsType } from './models';
import styles from './styles.scss';

const c = classGenerator(styles);

const FormField: FC<PropsType> = (props) => {
  const { name, value, title, error, disabled, onChange, placeholder, isEdit } = props;

  const inputChangeHandler = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    onChange(e.currentTarget.name, e.currentTarget.value);
  }, [onChange]);

  return (
    <div className={c('form-field')}>
      <div className={c('form-field__title')}>{title}</div>

      {isEdit ? (
        <>
          <input
            className={c('form-field__input')}
            value={value}
            type="input"
            onChange={inputChangeHandler}
            placeholder={placeholder || ''}
            name={name}
            disabled={disabled}
          />
          {error && <div className={c('form-field__error-msg')}>{error}</div>}
        </>
      ) : <div className={c('form-field__value')}>{value}</div>
      }
    </div>
  );
};

export default memo(FormField);
