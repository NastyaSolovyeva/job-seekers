import React, { FC, memo, useCallback, useMemo } from 'react';

import { classGenerator } from '@helpers';

import { PropsType } from './models';
import styles from './styles.scss';

const c = classGenerator(styles);

const Button: FC<PropsType> = (props) => {
  const { disabled = false,
    children = '',
    onClick,
    type = 'button',
    appearance = 'primary',
    className = '' } = props;

  const classes = useMemo(() => c(c('button', `button_${appearance}`, className)),
    [appearance, className]);

  const clickHandler = useCallback((e: React.MouseEvent) => onClick && onClick(e), [onClick]);

  return (
    <button
      className={classes}
      type={type}
      onClick={clickHandler}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default memo(Button);
