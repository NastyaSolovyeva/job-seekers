import React, { ReactNode } from 'react';

export type PropsType = {
  size?: 's' | 'm' | 'l',
  onClick?: (e: React.MouseEvent) => void,
  className?: string,
  children?: ReactNode,
  type?: 'submit' | 'reset' | 'button',
  appearance?: 'primary' | 'secondary',
  disabled?: boolean
}
