import React, { FC, memo } from 'react';

import { classGenerator } from '@helpers';

import styles from './styles.scss';

const c = classGenerator(styles);

export const Loader: FC = () => (
  <div className={c('loader-wrapper')}>
    <div className={c('loader')}>
      <div />
      <div />
      <div />
      <div />
    </div>
  </div>
);

export default memo(Loader);
