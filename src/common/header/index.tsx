import React, { FC, memo } from 'react';

import { classGenerator } from '@helpers';

import { PropsType } from './models';
import styles from './styles.scss';

const c = classGenerator(styles);

const Header: FC<PropsType> = (props) => {
  const { title, sideContent = '', className = '', subtitle } = props;

  return (
    <div className={c('header', className)}>
      <div className={c('header__title')}>
        <div className={c('header__title-text')}>
          {title}
        </div>
        <div className={c('header__subtitle')}>
          {subtitle}
        </div>
      </div>

      <div className={c('header__side-content')}>{sideContent}</div>
    </div>
  );
};

export default memo(Header);
