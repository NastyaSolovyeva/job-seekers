import { ReactNode } from 'react';

export type PropsType = {
  title: string,
  className?: string,
  subtitle?: string,
  sideContent?: ReactNode
}
