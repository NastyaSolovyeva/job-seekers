import React from 'react';
import * as ReactDOM from 'react-dom';
import { applyMiddleware, createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import createSagaMiddleware from 'redux-saga';

import UsersLayout from '@routes/users/layout';
import Profile from '@routes/users/profile';
import { watchSagas } from '@services/sagas';
import usersReducer from '@services/users/reducer';
import { classGenerator } from '@helpers';
import Header from '@routes/header';

import styles from './styles.scss';

const c = classGenerator(styles);

const sagaMiddleware = createSagaMiddleware();
const enhancer = applyMiddleware(sagaMiddleware);

const reducer = combineReducers({
  usersReducer
});

const store = createStore(reducer, enhancer);

sagaMiddleware.run(watchSagas);

const App = () => (
  <>
    <Header />
    <div className={c('container')}>
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={UsersLayout} />
            <Route exact path="/user/:id" component={Profile} />
          </Switch>
        </BrowserRouter>
      </Provider>
    </div>
  </>
);

const root = document.getElementById('root');

ReactDOM.render(<App />, root);
